/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ORM;

import entities.Article;
import entities.Journalist;
import entities.Tag;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author vincentVITON
 */
public class MockDbAccess implements DbAccessor {
    
    public List<Tag> getAllTags() throws Exception{
        
        Tag tag1 = new Tag("testTag1");
        Tag tag2 = new Tag("testTag2");
        
        return asList(tag1,tag2) ;

    }
    
     public Journalist checkJournalist(String firstName, String lastName, String password)throws Exception{
         return new Journalist();
     }
    
     public  int addArticle(Article article, Journalist journalist) throws Exception { return 0;}

    @Override
    public int getTagIdByLabel(String label) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int addTag(Tag tag) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTagReferenceToArticle(int tagId, int articleId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Article> getAllArticles() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Article getArticle(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
