/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ORM;

/**
 *
 * @author vincentVITON
 */
import entities.Article;
import entities.Journalist;
import entities.Tag;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JDBCConnexion {

    //Version Wamp
    public static String URL = "jdbc:mysql://localhost:3308/afp";
    public static final String LOGIN = "root";
    public static final String PASSWORD = "123";

    private final static String QUERY_GET_ALL_TAGS = "SELECT * FROM tag";

    public static List<Tag> getAllTags() throws Exception {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            stmt = con.createStatement();
            ResultSet rset = stmt.executeQuery(QUERY_GET_ALL_TAGS);
            List<Tag> allTags = new ArrayList<>();
            while (rset.next()) {
                Tag tag = rsetToTag(rset);
                allTags.add(tag);
            }
            return allTags;
        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Article getArticle(int id) throws Exception {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            stmt = con.createStatement();
            String QUERY = "select  news.id,news.title, news.content, news.creationDate, news.id_Journalist, tag.label, journalist.credit "
                    + "	from news "
                    + "	inner join refer on refer.id_News=news.id "
                    + " inner join tag on refer.id = tag.id "
                    + " inner join journalist on news.id_Journalist = journalist.id "
                    + " where news.id =" + id
                    + " ;";
            ResultSet rset = stmt.executeQuery(QUERY);
            List<Article> allArticles = new ArrayList();

            Article article = new Article();
            while (rset.next()) {
                if (article.getId() == 0) {
                    article = rsetToArticle(rset);
                }

                article.addTag(new Tag(rset.getString("label")));
            }

            return article;

        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<Article> getAllArticles() throws Exception {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            stmt = con.createStatement();
            String QUERY = "select  news.id,news.title, news.content, news.creationDate, news.id_Journalist, tag.label, journalist.credit "
                    + "	from news "
                    + "	inner join refer on refer.id_News=news.id "
                    + " inner join tag on refer.id = tag.id "
                    + " inner join journalist on news.id_Journalist = journalist.id "
                    + " ORDER BY news.id "
                    + " ;";
            ResultSet rset = stmt.executeQuery(QUERY);
            List<Article> allArticles = new ArrayList();
            int lastArticleId = -1;
            Article article = new Article();
            while (rset.next()) {

                if (rset.getInt("id") != lastArticleId) {
                    lastArticleId = rset.getInt("id");
                    article = rsetToArticle(rset);
                    allArticles.add(article);
                }

                article.addTag(new Tag(rset.getString("label")));
            }
            return allArticles;
        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Journalist checkJournalist(String firstName, String lastName, String password) throws Exception {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            stmt = con.createStatement();
            //ResultSet rset= stmt.executeQuery(QUERY_GET_USER_ID_FROM_NAME+pseudo+"'");
            String queryString = "SELECT * FROM journalist WHERE firstname='" + firstName + "' and lastname='" + lastName + "' and password='" + password + "';";

            ResultSet rset = stmt.executeQuery(queryString);
            rset.next();

            return rsetToJournalist(rset);

        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void addTagReferenceToArticle(int tagId, int articleId) throws Exception {

        Connection con = null;
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            String QUERY = "INSERT INTO refer (id,id_News) VALUES(" + tagId + "," + articleId + ")";
            stmt = con.createStatement();
            stmt.executeUpdate(QUERY);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static int getTagIdByLabel(String label) throws Exception {

        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            stmt = con.createStatement();
            String QUERY = "SELECT * FROM tag WHERE label='";
            ResultSet rset = stmt.executeQuery(QUERY + label + "'");
            if (rset.next()) {
                return rset.getInt("id");
            } else {
                return -1;
            }

        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static int addTag(Tag tag) throws Exception {

        Connection con = null;
        PreparedStatement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            String stringQuery = "INSERT INTO tag(label) VALUES (?)";
            stmt = con.prepareStatement(stringQuery, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, tag.getLabel());

            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                return -1;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static int addArticle(Article article, Journalist journalist) throws Exception {

        Connection con = null;
        PreparedStatement stmt = null;
        //System.out.print("ON PASSE PAR JDBC addArticle");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
            String stringQuery = "INSERT INTO news(title,content,creationDate,id_Journalist) VALUES (?,?,?,?)";
            stmt = con.prepareStatement(stringQuery, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getContent());
            stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            stmt.setInt(4, journalist.getId());
            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                return -1;
            }

        } catch (Exception e) {
            //  System.out.print("Mais ça plante");
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (con != null) {// On ferme la connexion
                try {
                    con.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*	
private final static String QUERY_ADD_USER = "INSERT INTO users(pseudo,lastActivityTime) VALUES (?, ?);";
	
	public static void addUser(User user) throws Exception {
		//System.out.println("In ConnexionBDD");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_ADD_USER);
			// Remplir la requ�te
			stmt.setString(1, user.pseudo);
			stmt.setLong(2, user.lastActivityTime);
			// Lancer la requ�te
			stmt.executeUpdate();
			
			//System.out.println(user.pseudo);
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	

	
private final static String QUERY_UPDATE_USER_TIME = "UPDATE users SET lastActivityTime=? where id=?;";
	
	public static void updateUserTime(String username) throws Exception {
		//System.out.println("In ConnexionBDD USERTIME");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_UPDATE_USER_TIME);
			// Remplir la requ�te
			stmt.setLong(1, new Date().getTime());
			stmt.setInt(2, getUserIdFromName (username));
			// Lancer la requ�te
			stmt.executeUpdate();
			
		
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_ADD_MESSAGE = "INSERT INTO messages(userID, content, time, iswizz) VALUES (?, ?, ?, ?);";
	
	public static void addMessage(Message message) throws Exception {
		//System.out.println("In ConnexionBDD Add Message");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_ADD_MESSAGE);
			// Remplir la requ�te
			stmt.setInt(1, getUserIdFromName(message.user.pseudo));
			stmt.setString(2, message.content);
			stmt.setLong(3, message.time);
			stmt.setBoolean(4, message.isWizz);
			// Lancer la requ�te
			stmt.executeUpdate();
			
			//System.out.println(message.content);
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private final static String QUERY_GET_USER_ID_FROM_NAME= "SELECT * FROM users WHERE pseudo='";
	public static int getUserIdFromName(String pseudo) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//Pour travailler avec Tomcat et wamp Rajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_USER_ID_FROM_NAME+pseudo+"'");
			rset.next();
			return rset.getInt("id");
			
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_GET_USER_FROM_ID= "SELECT * FROM users WHERE id=";
	public static User getUserFromId(int id) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//Pour travailler avec Tomcat et wamp Rajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_USER_FROM_ID+id);
			rset.next();
			return rsetToUser(rset);
			
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_GET_RECENT_MESSAGES= "SELECT * FROM messages where time>";
	public static ArrayList<Message> getAllMessagesSince(long time) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//System.out.println("IN GET RECENT MESSAGES");
			//System.out.println(time);
			//Pour travailler avec Tomcatet wampRajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_RECENT_MESSAGES+time);
			ArrayList<Message> returnList = new ArrayList<>();
			while(rset.next()) {
			Message message = rsetToMessage(rset);
			returnList.add(message);
			}
	
			return returnList;
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private final static String QUERY_GET_ACTIVE_USERS= "SELECT * FROM users where lastActivityTime > ";
	
	public static ArrayList<String> getActiveUsers() throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); 
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_ACTIVE_USERS+(new Date().getTime()-60000));
			ArrayList<String> returnList = new ArrayList<>();
			while(rset.next()) {
			User user = rsetToUser(rset);
			returnList.add(user.pseudo);
			}
			return returnList;
		}
		finally{
			if(con!= null) {
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static Tag rsetToTag(ResultSet rset) throws SQLException {

        return new Tag(rset.getString("label"));
    }

    private static Journalist rsetToJournalist(ResultSet rset) throws SQLException {

        Journalist journalist = new Journalist();
        journalist.setId(rset.getInt("id"));
        journalist.setFirstname(rset.getString("firstname"));
        journalist.setLastname(rset.getString("lastname"));
        journalist.setCredit(rset.getInt("credit"));

        return journalist;
    }

    private static Article rsetToArticle(ResultSet rset) throws SQLException {

        Article article = new Article();
        article.setId(rset.getInt("id"));
        article.setTitle(rset.getString("title"));
        article.setContent(rset.getString("content"));
        article.setCreationDate(rset.getTimestamp("creationDate"));
        article.setJournalistId(rset.getInt("id_Journalist"));
        article.setCredibility(rset.getInt("credit"));
        return article;
    }

}
