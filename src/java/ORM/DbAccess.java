/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ORM;

import entities.Article;
import entities.Journalist;
import entities.Tag;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author vincentVITON
 */
public class DbAccess implements DbAccessor {

    public List<Tag> getAllTags() throws Exception {
        return JDBCConnexion.getAllTags();
    }

    public Journalist checkJournalist(String firstName, String lastName, String password) throws Exception {
        return JDBCConnexion.checkJournalist(firstName, lastName, password);
    }

    public int addArticle(Article article, Journalist journalist) throws Exception {
        return JDBCConnexion.addArticle(article, journalist);
    }

    @Override
    public int getTagIdByLabel(String label) throws Exception {
         return JDBCConnexion.getTagIdByLabel(label);
    }

    @Override
    public int addTag(Tag tag) throws Exception {
        return JDBCConnexion.addTag(tag);
    }

    @Override
    public void addTagReferenceToArticle(int tagId, int articleId) throws Exception {
        JDBCConnexion.addTagReferenceToArticle(tagId,articleId);
    }

    @Override
    public List<Article> getAllArticles() throws Exception {
         return JDBCConnexion.getAllArticles();
    }

    @Override
    public Article getArticle(int id) throws Exception {
        return JDBCConnexion.getArticle(id);
    }
}
