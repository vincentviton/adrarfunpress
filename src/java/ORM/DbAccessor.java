/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ORM;

import entities.Article;
import entities.Journalist;
import entities.Tag;
import java.util.List;

/**
 *
 * @author vincentVITON
 */
public interface DbAccessor  {
    
    public List<Tag> getAllTags() throws Exception;
    public Journalist checkJournalist(String firstName, String lastName, String password)throws Exception;
    public int addArticle(Article article, Journalist journalist) throws Exception ;
    public int getTagIdByLabel(String label) throws Exception;
    public int addTag (Tag tag) throws Exception;
    public void addTagReferenceToArticle(int tagId, int articleId ) throws Exception;
    public List<Article> getAllArticles() throws Exception;
    public Article getArticle(int id) throws Exception;
}
