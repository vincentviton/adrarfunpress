/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vincentVITON
 */
public class Article {

    private int id;
    private String title;
    private String content;
    private Timestamp creationDate;
    private List<Tag> tags;
    private int journalistId;
    private int credibility;

    public int getCredibility() {
        return credibility;
    }

    public void setCredibility(int credibility) {
        this.credibility = credibility;
    }

    public Article() {
        tags = new ArrayList();
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public int getJournalistId() {
        return journalistId;
    }

    public void setJournalistId(int journalistId) {
        this.journalistId = journalistId;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTagsList(List<Tag> tagsList) {
        this.tags = tagsList;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

  

}
