package entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vincentVITON
 */
public class Journalist {
    
    private int id;
    private String firstname;
    private String lastname;
    private int credit;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

  
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getFullname(){
        
        return firstname.substring(0, 1).toUpperCase() + firstname.substring(1)
                +" "+
                lastname.toUpperCase();
    }
    
}
