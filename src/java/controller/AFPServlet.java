package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import ORM.DbAccess;
import ORM.DbAccessor;
import ORM.MockDbAccess;
import entities.Article;
import entities.Journalist;
import entities.Tag;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author vincentVITON
 */
//@WebServlet(urlPatterns = {"/AFPServlet"})
public class AFPServlet extends HttpServlet {

    final public static String TRUE = "true";
    final public static String FALSE = "false";

    final public static String LOGIN_VIEW = "/WEB-INF/jsp/login.jsp";
    final public static String HOME_VIEW = "/WEB-INF/jsp/home.jsp";
    final public static String ARTICLE_VIEW = "/WEB-INF/jsp/article.jsp";

    final public static String NEW_ARTICLE_REQUEST_PARAM = "newArticle";
    final public static String SUBMIT_ARTICLE_REQUEST_PARAM = "submitNewArticle";
    final public static String GET_RECENT_ARTICLES_REQUEST_PARAM = "getRecentArticlesList";

    final public static String FIRSTNAME_REQUEST_PARAM = "firstname";
    final public static String LASTNAME_REQUEST_PARAM = "lastname";
    final public static String PASSWORD_REQUEST_PARAM = "password";

    final public static String TITLE_REQUEST_PARAM = "title";
    final public static String CONTENT_REQUEST_PARAM = "content";
    final public static String TAGS_REQUEST_PARAM = "tags";

    final public static String LOGIN_REQUEST_PARAM = "login";
    final public static String LOGOUT_REQUEST_PARAM = "logout";
    final public static String ALL_TAGS_REQUEST_ATTR = "allTags";
    final public static String ALL_ARTICLES_REQUEST_ATTR = "allArticles";

    final public static String JOURNALIST_REQUEST_ATTR = "journalist";

    final private DbAccessor dbAccess = new DbAccess();
    //final private DbAccessor dbAccess = new MockDbAccess();

    //final public static String FULLNAME_REQUEST_ATTR = "fullname";
    //final public static String USER_CREDIT_REQUEST_ATTR = "credit";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private List<Article> getAllArticlesSortedByDate() {
        
        List<Article> allArticles;
        allArticles = getAllArticles();

        Collections.sort(allArticles, new Comparator<Article>() {
            @Override
            public int compare(Article article2, Article article1) {

                return article1.getCreationDate().compareTo(article2.getCreationDate());
            }
        });

        return allArticles;
    }

    private List<Article> getAllArticles() {

        try {
            return dbAccess.getAllArticles();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        ServletContext context = this.getServletContext();
        RequestDispatcher dispatcher = context.getRequestDispatcher(HOME_VIEW);
        
        /////////////////////
        // SHOW ARTICLE
        
        if (request.getParameter("articleId")!=null){
            //System.out.println(request.getParameter("articleiD"));
            try{
                Article article = dbAccess.getArticle(parseInt(request.getParameter("articleId")));
                request.setAttribute("article", article);
            }
           catch (Exception e) {
            e.printStackTrace();
           
        }
            
            
            dispatcher = context.getRequestDispatcher(ARTICLE_VIEW);
            
            //request.setAttribute("article",request.getParameter("article"));
        }
        

        ////////////////////////////////////////////////////////////////////////////////
        // GET ALL ARTICLES
        if (TRUE.equals(request.getParameter(GET_RECENT_ARTICLES_REQUEST_PARAM))) {
            try {
                request.setAttribute(ALL_ARTICLES_REQUEST_ATTR, getAllArticlesSortedByDate());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////////////////////////////////////////////////
        // ADD NEW ARTICLE
        if (TRUE.equals(request.getParameter(SUBMIT_ARTICLE_REQUEST_PARAM))) {
            Article article = new Article();

            article.setTitle(request.getParameter(TITLE_REQUEST_PARAM));
            article.setContent(request.getParameter(CONTENT_REQUEST_PARAM));
            try {
                int newArticleId = dbAccess.addArticle(article, (Journalist) session.getAttribute(JOURNALIST_REQUEST_ATTR));

                String tagsString = request.getParameter(TAGS_REQUEST_PARAM);

                final List<String> asList = Arrays.asList(tagsString.split(","));
                List<String> tagsList = new ArrayList(asList);

                for (String tagLabel : tagsList) {

                    tagLabel = tagLabel.trim();
                    int tagId = dbAccess.getTagIdByLabel(tagLabel);

                    if (tagId == -1) {
                        Tag tag = new Tag(tagLabel);
                        tagId = dbAccess.addTag(tag);
                    }
                    dbAccess.addTagReferenceToArticle(tagId, newArticleId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // redirect sur article page
        if (TRUE.equals(request.getParameter(NEW_ARTICLE_REQUEST_PARAM))) {
            dispatcher = context.getRequestDispatcher(ARTICLE_VIEW);

        }

        /////////// Afficher les tags /////
        try {
            List<Tag> allTags = dbAccess.getAllTags();
            request.setAttribute(ALL_TAGS_REQUEST_ATTR, allTags);

        } catch (final Exception e) {
            e.printStackTrace();
        }

/////// logout
        if (TRUE.equals(request.getParameter(LOGOUT_REQUEST_PARAM))) {
            session.removeAttribute(JOURNALIST_REQUEST_ATTR);
        }

        //// login
        if (TRUE.equals(request.getParameter(LOGIN_REQUEST_PARAM))) {
            dispatcher = context.getRequestDispatcher(LOGIN_VIEW);

        } else {
            if (request.getParameter(FIRSTNAME_REQUEST_PARAM) != null && request.getParameter(LASTNAME_REQUEST_PARAM) != null && request.getParameter(PASSWORD_REQUEST_PARAM) != null) {

                String firstname = request.getParameter(FIRSTNAME_REQUEST_PARAM);
                String lastname = request.getParameter(LASTNAME_REQUEST_PARAM);
                String password = request.getParameter(PASSWORD_REQUEST_PARAM);
                Journalist journalist = null;
                try {
                    journalist = dbAccess.checkJournalist(firstname, lastname, password);
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                if (journalist != null) {
                    session.setAttribute(JOURNALIST_REQUEST_ATTR, journalist);
                }
                dispatcher = context.getRequestDispatcher(HOME_VIEW);
            }
        }

        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
