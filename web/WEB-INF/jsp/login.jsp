<%-- 
    Document   : login
    Created on : 30 oct. 2018, 11:49:00
    Author     : vincentVITON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic_page>
    <jsp:attribute name="header">
      <title>Login Page</title>
    </jsp:attribute>
    <jsp:attribute name="footer">
      
    </jsp:attribute>
    <jsp:body>
        <h1>Please Login</h1>
        <form method="post">
            <label class="col-form-label col-form-label-lg" for="inputFirstName">Prénom</label>
            <input class="form-control form-control-lg" placeholder="Votre prénom" id="inputFirstName" type="text" name="firstname">
            <label class="col-form-label col-form-label-lg" for="inputLastName">Nom</label>
            <input class="form-control form-control-lg" placeholder="Votre nom" id="inputLastName" type="text" name="lastname">
            <label class="col-form-label col-form-label-lg" for="inputPassword">Mot de passe</label>
            <input class="form-control form-control-lg" placeholder="Votre mot de passe" id="inputPassword" type="text" name="password">
            <button type="submit" class="btn btn-outline-success">Submit</button>
        </form>
    </jsp:body>
</t:generic_page>
