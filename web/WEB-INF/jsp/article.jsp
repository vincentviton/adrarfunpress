<%-- 
    Document   : login
    Created on : 30 oct. 2018, 11:49:00
    Author     : vincentVITON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic_page>
    <jsp:attribute name="header">
        <title>Article</title>
    </jsp:attribute>
    <jsp:attribute name="footer">

    </jsp:attribute>
    <jsp:body>
        <h1>Article</h1>
        <c:if test="${article==null}" >
            <form method="post">

                <div class="form-group">
                    <label class="col-form-label col-form-label-lg" for="inputTitre">Titre</label>
                    <input class="form-control form-control-lg" placeholder="Titre de l'article" id="inputTitre" type="text" name="title">
                </div>

                <div class="form-group">
                    <label class="col-form-label col-form-label-lg" for="inputContent">Contenu</label>
                    <textarea class="form-control" id="exampleTextarea" rows="4" id="inputContent" name="content" placeholder="Contenu de l'article">   </textarea>
                </div>

                <div class="form-group">
                    <label class="col-form-label col-form-label-lg" for="inputTags">Tags</label>
                    <input class="form-control form-control-lg" placeholder="Tags (séparés par des virgules)" id="inputTags" type="text" name="tags">
                </div>

                <input type="hidden" name="submitNewArticle" value="true" /> 

                <button type="submit" class="btn btn-outline-success">Submit</button>
            </form>
        </c:if> 
        <c:if test="${article!=null}" >
            <h2>${article.title}</h2>
            <h3>${article.creationDate}</h3>
            <p>${article.content}</p>
            Tags : 
            <p>
                <c:forEach items="${ article.getTags() }" var="tag" >
                    ${ tag.label }  
                </c:forEach>

            </p>


        </c:if>
    </jsp:body>
</t:generic_page>