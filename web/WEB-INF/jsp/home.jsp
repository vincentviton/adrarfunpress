<%-- 
    Document   : home
    Created on : 30 oct. 2018, 11:44:33
    Author     : vincentVITON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<t:generic_page>
    <jsp:attribute name="header">
        <title>ADRAR FUN PRESS HOME PAGE</title>
    </jsp:attribute>
    <jsp:attribute name="footer">

    </jsp:attribute>
    <jsp:body>

        <c:if test="${sessionScope.journalist!=null}" >
            <h2>Vous êtes connecté en tant que ${journalist.getFullname()} </h2>
            <h2>Vous pouvez ajouter un article </h2>
            <h2>Vous pouvez valider les articles </h2>
            <br>
        </c:if>




        <div style="border: solid black 1px">
            <h3>TAGS</h3>
            <c:forEach items="${ allTags }" var="tag" >
                ${ tag.label }
            </c:forEach>
        </div>

        <c:if test="${allArticles!=null}" >
            <div style="border: solid black 1px">
                <h3>ARTICLES</h3>

                <c:forEach items="${ allArticles }" var="article" >

                    <form id="articleRequestForm+${article.id}" method="post" >
                        <input type="hidden" name="articleId" value=${article.id} /> 
                        <a class="nav-link" style="cursor: pointer;"  onclick="document.getElementById('articleRequestForm+${article.id}').submit();">
                            <div>
                                Titre : 
                                ${ article.title }
                                - Date :
                                ${ article.creationDate }
                                - Tags :
                                <c:forEach items="${ article.getTags() }" var="tag" >
                                    ${ tag.label }  
                                </c:forEach>
                            </div>
                        </a>
                    </form>


                </c:forEach>

            </div>
        </c:if>   



        <br>










    </jsp:body>
</t:generic_page>