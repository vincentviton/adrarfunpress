<%-- 
    Document   : generic_page
    Created on : 30 oct. 2018, 13:59:54
    Author     : vincentVITON
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<html>
    <body>
        <div id="pageheader">
            <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="#">AFP</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <form id="recentArticlesListRequestForm" method="post" >
                                <input type="hidden" name="getRecentArticlesList" value="true" /> 
                                <a class="nav-link" style="cursor: pointer;" onclick="document.getElementById('recentArticlesListRequestForm').submit();">Nouveaux Articles</a>
                            </form>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Meilleurs Articles</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Auteurs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>

                        <c:if test="${sessionScope.journalist==null}" >
                            <li class="nav-item">
                                <form id="loginRequestForm" method="post" >
                                    <input type="hidden" name="login" value="true" /> 
                                    <a class="nav-link" style="cursor: pointer;" onclick="document.getElementById('loginRequestForm').submit();">Login</a>
                                </form>
                            </li>
                        </c:if>



                        <c:if test="${sessionScope.journalist!=null}" >
                            <li class="nav-item">
                                <form id="logoutRequestForm" method="post" >
                                    <input type="hidden" name="logout" value="true" /> 
                                    <a class="nav-link" style="cursor: pointer;" onclick="document.getElementById('logoutRequestForm').submit();">Logout</a>
                                </form>
                            </li>
                            <li class="nav-item">
                                <form id="newArticleForm" method="post" >
                                    <input type="hidden" name="newArticle" value="true" /> 
                                    <a class="nav-link" style="cursor: pointer;" onclick="document.getElementById('newArticleForm').submit();">Ajouter Article</a>
                                </form>
                            </li>
                        </c:if>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search">
                        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
            <jsp:invoke fragment="header"/>
        </div>


        <div id="body">
            <jsp:doBody/>
        </div>


        <div id="pagefooter">
            <jsp:invoke fragment="footer"/>
            <p id="copyright">Copyright 2018, Vincent Viton.</p>
        </div>
    </body>
</html>